﻿using UnityEditor;
using UnityEngine;

// ReSharper disable CheckNamespace
namespace UnityModule.ProgressBar.Editor
{
    [CustomEditor(typeof(ProgressBar))]
    public sealed class ProgressBarEditor : UnityEditor.Editor
    {
        #region properties

        private SerializedProperty _current;
        private SerializedProperty _maxValue;
        private SerializedProperty _direction;
        private SerializedProperty _mode;

        private SerializedProperty _delayRunForeground;
        private SerializedProperty _durationForeground;
        private SerializedProperty _oneUnitForegroundValue;
        private SerializedProperty _percentIncreaseDuration;
        private SerializedProperty _isCustomForegroundEase;
        private SerializedProperty _easeForeground;
        private SerializedProperty _curveForeground;

        private SerializedProperty _isDelayBar;
        private SerializedProperty _delayRunDelayBar;
        private SerializedProperty _durationDelayBar;
        private SerializedProperty _oneUnitDelayBarValue;
        private SerializedProperty _percentIncreaseDurationDelay;
        private SerializedProperty _isCustomDelayedEase;
        private SerializedProperty _easeDelayed;
        private SerializedProperty _curveDelayed;

        private SerializedProperty _delayedBar;
        private SerializedProperty _foregroundBar;
        private bool _foldoutForeground = true;
        private bool _foldoutDelayed = true;

        #endregion

        private void OnEnable()
        {
            Init();
        }

        private void Init()
        {
            _current = serializedObject.FindProperty("current");
            _maxValue = serializedObject.FindProperty("maxValue");
            _direction = serializedObject.FindProperty("direction");
            _mode = serializedObject.FindProperty("mode");
            _delayRunForeground = serializedObject.FindProperty("delayRunForeground");
            _durationForeground = serializedObject.FindProperty("durationForeground");
            _oneUnitForegroundValue = serializedObject.FindProperty("oneUnitForegroundValue");
            _percentIncreaseDuration = serializedObject.FindProperty("percentIncreaseDuration");
            _isCustomForegroundEase = serializedObject.FindProperty("isCustomForegroundEase");
            _easeForeground = serializedObject.FindProperty("easeForeground");
            _curveForeground = serializedObject.FindProperty("curveForeground");
            _isDelayBar = serializedObject.FindProperty("isDelayBar");
            _delayRunDelayBar = serializedObject.FindProperty("delayRunDelayBar");
            _durationDelayBar = serializedObject.FindProperty("durationDelayBar");
            _oneUnitDelayBarValue = serializedObject.FindProperty("oneUnitDelayBarValue");
            _percentIncreaseDurationDelay = serializedObject.FindProperty("percentIncreaseDurationDelay");
            _isCustomDelayedEase = serializedObject.FindProperty("isCustomDelayedEase");
            _easeDelayed = serializedObject.FindProperty("easeDelayed");
            _curveDelayed = serializedObject.FindProperty("curveDelayed");
            _delayedBar = serializedObject.FindProperty("delayedBar");
            _foregroundBar = serializedObject.FindProperty("foregroundBar");
        }

        #region Overrides of Editor

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            GlobalSetup();
            GUI.contentColor = new Color(0.86f, 0.23f, 0.11f);
            _foldoutForeground = EditorGUILayout.Foldout(_foldoutForeground, "Foreground");
            GUI.contentColor = Color.white;
            DrawForeground();
            if (_isDelayBar.boolValue)
            {
                GUI.contentColor = new Color(0.85f, 0.44f, 0.21f, 0.62f);
                _foldoutDelayed = EditorGUILayout.Foldout(_foldoutDelayed, "Delayed");
                GUI.contentColor = Color.white;
                DrawDelayed();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawForeground()
        {
            GUILayout.Space(2);
            if (!_foldoutForeground) return;
            EditorGUILayout.PropertyField(_delayRunForeground, new GUIContent("Delay (ms)"));
            EditorGUILayout.PropertyField(_durationForeground, new GUIContent("Duration (s)"));
            EditorGUILayout.PropertyField(_oneUnitForegroundValue, new GUIContent("Value One Unit"));
            EditorGUILayout.PropertyField(_percentIncreaseDuration, new GUIContent("Percent Increase (%)"));
            EditorGUILayout.PropertyField(_isCustomForegroundEase, new GUIContent("Use Curve"));
            if (_isCustomForegroundEase.boolValue)
            {
                EditorGUILayout.PropertyField(_curveForeground, new GUIContent("Curve"));
            }
            else
            {
                EditorGUILayout.PropertyField(_easeForeground, new GUIContent("Ease"));
            }
        }

        private void DrawDelayed()
        {
            GUILayout.Space(2);
            if (!_foldoutDelayed) return;
            EditorGUILayout.PropertyField(_delayRunDelayBar, new GUIContent("Delay (ms)"));
            EditorGUILayout.PropertyField(_durationDelayBar, new GUIContent("Duration (s)"));
            EditorGUILayout.PropertyField(_oneUnitDelayBarValue, new GUIContent("Value One Unit"));
            EditorGUILayout.PropertyField(_percentIncreaseDurationDelay, new GUIContent("Percent Increase (%)"));
            EditorGUILayout.PropertyField(_isCustomDelayedEase, new GUIContent("Use Curve"));
            if (_isCustomDelayedEase.boolValue)
            {
                EditorGUILayout.PropertyField(_curveDelayed, new GUIContent("Curve"));
            }
            else
            {
                EditorGUILayout.PropertyField(_easeDelayed, new GUIContent("Ease"));
            }
        }

        private void GlobalSetup()
        {
            GUILayout.Space(8);
            var position = EditorGUILayout.GetControlRect();
            var barPosition = new Rect(position.position.x, position.position.y, position.size.x, EditorGUIUtility.singleLineHeight);
            var barLabel = $"{_current.floatValue:0.00}" + "/" + _maxValue.floatValue;
            var fillPercentage = _current.floatValue / _maxValue.floatValue;
            if (fillPercentage > 1)
            {
                GUI.contentColor = new Color(1f, 0.25f, 0.13f);
                var align = GUI.skin.label.alignment;
                GUI.skin.label.alignment = TextAnchor.UpperCenter;
                GUILayout.Space(8);
                GUI.Label(new Rect(position.position.x, position.position.y + 18, position.size.x, EditorGUIUtility.singleLineHeight), "Property value is over max value has been specified!");
                GUILayout.Space(8);
                GUI.contentColor = Color.white;
                GUI.skin.label.alignment = align;
            }

            DrawBar(barPosition, Mathf.Clamp01(fillPercentage), barLabel, new Color(0f, 0.62f, 0.82f), Color.white);
            EditorGUILayout.PropertyField(_direction);
            EditorGUILayout.PropertyField(_mode);
            EditorGUILayout.PropertyField(_foregroundBar);
            if (_isDelayBar.boolValue)
            {
                EditorGUILayout.PropertyField(_delayedBar);
            }

            EditorGUILayout.PropertyField(_isDelayBar, new GUIContent("Have Delay Bar"));
        }

        private void DrawBar(Rect position, float fillPercent, string label, Color barColor, Color labelColor)
        {
            if (Event.current.type != EventType.Repaint)
            {
                return;
            }

            var fillRect = new Rect(position.x, position.y, position.width * fillPercent, position.height);

            EditorGUI.DrawRect(position, new Color(0.4f, 0.45f, 0.5f));
            EditorGUI.DrawRect(fillRect, barColor);

            // set alignment and cache the default
            var align = GUI.skin.label.alignment;
            GUI.skin.label.alignment = TextAnchor.UpperCenter;

            // set the color and cache the default
            var c = GUI.contentColor;
            GUI.contentColor = labelColor;

            // calculate the position
            var labelRect = new Rect(position.x, position.y - 2, position.width, position.height);

            // draw~
            EditorGUI.DropShadowLabel(labelRect, label);

            // reset color and alignment
            GUI.contentColor = c;
            GUI.skin.label.alignment = align;
        }

        #endregion
    }
}