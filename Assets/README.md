# Progress Bar

## Status
    - Breaking change and now it is one part of com.yenmoc.uniui


## Installation

```bash
"com.yenmoc.progress-bar":"https://gitlab.com/yenmoc/progress-bar"
or
npm publish --registry http://localhost:4873
```

## Usages

```csharp
        ProgressBar progressBar;
        progressBar.Initialized(min, max);
        progressBar.IncreaseGuard(value);
        progressBar.DecreaseGuard(value);    
        
        progressBar.Increase(value);
        progressBar.Decrease(value);
```
