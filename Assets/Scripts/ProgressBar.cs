﻿using System;
using ExtraUniRx;
using UniRx;
using UnityEngine;
using UnityEngine.Profiling;
using UnityModule.UniTween;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.ProgressBar
{
    public class ProgressBar : MonoBehaviour
    {
        #region enum

        public enum FillModes
        {
            LocalScale,
            FillAmount,
            Position
        }

        public enum BarDirections
        {
            LeftToRight,
            RightToLeft,
            UpToDown,
            DownToUp
        }

        #endregion

        #region properties

#pragma warning disable 0649
        [SerializeField] private float current;
        public TenseSubject<float> CurrentProgress { get; set; }
        [SerializeField] private float maxValue;
        [SerializeField] private float minValue;
        [SerializeField] private BarDirections direction = BarDirections.LeftToRight;
        [SerializeField] private FillModes mode = FillModes.LocalScale;
        [SerializeField] private RectTransform delayedBar;
        [SerializeField] private RectTransform foregroundBar;

        [SerializeField] private float delayRunForeground = 150f;
        [SerializeField] private float durationForeground = 0.2f;
        [SerializeField] private float oneUnitForegroundValue = 10f;
        [SerializeField] private float percentIncreaseDuration = 20f;
        [SerializeField] private bool isCustomForegroundEase;
        [SerializeField] private Easing.Type easeForeground = Easing.Type.InQuad;
        [SerializeField] private AnimationCurve curveForeground = AnimationCurve.Linear(0, 1, 1, 1);

        [SerializeField] private bool isDelayBar;
        [SerializeField] private float delayRunDelayBar = 150;
        [SerializeField] private float durationDelayBar = 0.2f;
        [SerializeField] private float oneUnitDelayBarValue = 10f;
        [SerializeField] private float percentIncreaseDurationDelay = 20f;
        [SerializeField] private bool isCustomDelayedEase;
        [SerializeField] private Easing.Type easeDelayed = Easing.Type.InQuad;
        [SerializeField] private AnimationCurve curveDelayed = AnimationCurve.Linear(0, 1, 1, 1);

        private bool _isInitialized;
        private IDisposable _disposableForegroundWhenDo;
        private IDisposable _disposableForegroundWhenDid;
        private IDisposable _disposableDelayedWhenDo;
        private IDisposable _disposableDelayedWhenDid;

#pragma warning restore 0649

        #endregion

        #region function

        // ReSharper disable once MemberCanBePrivate.Global
        /// <summary>
        /// Initialize for progress bar
        /// </summary>
        /// <param name="min">value min</param>
        /// <param name="max">value max</param>
        public void Initialized(float min, float max)
        {
            if (_isInitialized)
            {
                return;
            }

            _isInitialized = true;
            _disposableForegroundWhenDo?.Dispose();
            _disposableDelayedWhenDo?.Dispose();
            _disposableForegroundWhenDid?.Dispose();
            _disposableDelayedWhenDid?.Dispose();
            CurrentProgress = new TenseSubject<float>();
            maxValue = max;
            minValue = min;
            current = max;
            var tempValue = Vector2.one;

            switch (mode)
            {
                case FillModes.LocalScale:
                    InitializedLocalScale(min, tempValue);
                    break;
                case FillModes.FillAmount:
                    InitializedFillAmount(min, tempValue);
                    break;
                case FillModes.Position:
                    InitializedPosition(min, tempValue);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void InitializedPosition(float min, Vector2 tempValue)
        {
            var rectTransform = GetComponent<RectTransform>();
            var startPosition = Vector2.zero;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (direction)
            {
                case BarDirections.LeftToRight:
                    current = min;
                    startPosition = new Vector2(-rectTransform.rect.width, 0);
                    break;
                case BarDirections.DownToUp:
                    current = min;
                    startPosition = new Vector2(0, -rectTransform.rect.height);
                    break;
            }

            UpdateValue(tempValue, new Vector2(0.5f, 0.5f));
            delayedBar.localPosition = startPosition;
            foregroundBar.localPosition = startPosition;

            _disposableForegroundWhenDid = CurrentProgress
                .WhenDid()
                .Throttle(TimeSpan.FromMilliseconds(delayRunForeground))
                .Subscribe(_ =>
                {
                    UpdatePosition(current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        rectTransform.rect,
                        foregroundBar,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });

            _disposableForegroundWhenDo = CurrentProgress
                .WhenDo()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdatePosition(current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        rectTransform.rect,
                        foregroundBar,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });

            if (!isDelayBar) return;

            _disposableDelayedWhenDo = CurrentProgress
                .WhenDo()
                .Throttle(TimeSpan.FromMilliseconds(delayRunDelayBar))
                .Subscribe(_ =>
                {
                    UpdatePosition(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        rectTransform.rect,
                        delayedBar,
                        isCustomDelayedEase,
                        easeDelayed,
                        curveDelayed);
                });
            _disposableDelayedWhenDid = CurrentProgress
                .WhenDid()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdatePosition(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        rectTransform.rect,
                        delayedBar,
                        isCustomDelayedEase,
                        easeDelayed,
                        curveDelayed);
                });
        }

        private void InitializedFillAmount(float min, Vector2 tempValue)
        {
            var fillAmount = 1;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (direction)
            {
                case BarDirections.LeftToRight:
                case BarDirections.DownToUp:
                    current = min;
                    fillAmount = 0;
                    break;
            }

            UpdateValue(tempValue, new Vector2(0.5f, 0.5f));
            var foregroundImage = foregroundBar.GetComponent<UnityEngine.UI.Image>();
            foregroundImage.fillAmount = fillAmount;

            _disposableForegroundWhenDid = CurrentProgress
                .WhenDid()
                .Throttle(TimeSpan.FromMilliseconds(delayRunForeground))
                .Subscribe(_ =>
                {
                    UpdateFillAmount(current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        foregroundImage,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });

            _disposableForegroundWhenDo = CurrentProgress
                .WhenDo()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdateFillAmount(current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        foregroundImage,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });

            if (!isDelayBar) return;

            var delayedBarImage = delayedBar.GetComponent<UnityEngine.UI.Image>();
            delayedBarImage.fillAmount = current;

            _disposableDelayedWhenDo = CurrentProgress
                .WhenDo()
                .Throttle(TimeSpan.FromMilliseconds(delayRunDelayBar))
                .Subscribe(_ =>
                {
                    UpdateFillAmount(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        delayedBarImage,
                        isCustomDelayedEase,
                        easeForeground,
                        curveForeground);
                });
            _disposableDelayedWhenDid = CurrentProgress
                .WhenDid()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdateFillAmount(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        delayedBarImage,
                        isCustomDelayedEase,
                        easeForeground,
                        curveForeground);
                });
        }

        private void InitializedLocalScale(float min, Vector2 tempValue)
        {
            Vector2 pivot;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (direction)
            {
                case BarDirections.RightToLeft:
                    pivot = new Vector2(0, 0.5f);
                    break;
                case BarDirections.DownToUp:
                    tempValue = Vector2.right;
                    pivot = new Vector2(0.5f, 0);
                    current = min;
                    break;
                case BarDirections.UpToDown:
                    pivot = new Vector2(0.5f, 0);
                    break;
                default: // BarDirections.LeftToRight
                    tempValue = Vector2.up;
                    pivot = new Vector2(0, 0.5f);
                    current = min;
                    break;
            }

            UpdateValue(tempValue, pivot);

            _disposableForegroundWhenDid = CurrentProgress
                .WhenDid()
                .Throttle(TimeSpan.FromMilliseconds(delayRunForeground))
                .Subscribe(_ =>
                {
                    UpdateLocalScale(
                        current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        foregroundBar,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });
            _disposableForegroundWhenDo = CurrentProgress
                .WhenDo()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdateLocalScale(
                        current,
                        CaculateDuration(_, oneUnitForegroundValue, percentIncreaseDuration, durationForeground),
                        foregroundBar,
                        isCustomForegroundEase,
                        easeForeground,
                        curveForeground);
                });

            if (!isDelayBar) return;

            _disposableDelayedWhenDo = CurrentProgress
                .WhenDo()
                .Throttle(TimeSpan.FromMilliseconds(delayRunDelayBar))
                .Subscribe(_ =>
                {
                    UpdateLocalScale(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        delayedBar,
                        isCustomDelayedEase,
                        easeDelayed,
                        curveDelayed);
                });
            _disposableDelayedWhenDid = CurrentProgress
                .WhenDid()
                .Subscribe(_ =>
                {
                    current += _;
                    UpdateLocalScale(
                        current,
                        CaculateDuration(_, oneUnitDelayBarValue, percentIncreaseDurationDelay, durationDelayBar),
                        delayedBar,
                        isCustomDelayedEase,
                        easeDelayed,
                        curveDelayed);
                });
        }

        private void UpdateValue(Vector2 vector2, Vector2 pivot)
        {
            foregroundBar.localScale = vector2;
            foregroundBar.pivot = pivot;
            if (!isDelayBar) return;
            delayedBar.localScale = vector2;
            delayedBar.pivot = pivot;
        }

        /// <summary>
        /// increase value of progress by value
        /// </summary>
        /// <param name="value">value increase</param>
        public void Increase(float value)
        {
            if (isDelayBar)
            {
                CurrentProgress.Did(value);
                return;
            }

            CurrentProgress.Do(value);
        }

        /// <summary>
        /// increase value of progress by value with guard keep value in range [min..max]
        /// </summary>
        /// <param name="value">value increase</param>
        public void IncreaseGuard(float value)
        {
            var valueIncrease = value;
            if (current + value > maxValue)
            {
                valueIncrease = maxValue - current;
                if (valueIncrease <= 0f)
                {
                    return;
                }
            }

            if (isDelayBar)
            {
                CurrentProgress.Did(valueIncrease);
                return;
            }

            CurrentProgress.Do(valueIncrease);
        }

        /// <summary>
        /// decrease value of progress by value
        /// </summary>
        /// <param name="value">value decrease</param>
        public void Decrease(float value)
        {
            CurrentProgress.Do(-value);
        }

        /// <summary>
        /// decrease value of progress by value with guard keep value in range [min..max]
        /// </summary>
        /// <param name="value">value decrease</param>
        public void DecreaseGuard(float value)
        {
            var valueDecrease = value;
            if (current - value < minValue)
            {
                valueDecrease = current;
                if (valueDecrease <= 0f)
                {
                    return;
                }
            }

            CurrentProgress.Do(-valueDecrease);
        }

        #region -- localscale ----------------------------------

        /// <summary>
        /// Update local scale gameobject
        /// </summary>
        /// <param name="_">param value</param>
        /// <param name="t">duration update scale</param>
        /// <param name="root">transform object update scale</param>
        /// <param name="isCustomEase"></param>
        /// <param name="ease"></param>
        /// <param name="curve"></param>
        private void UpdateLocalScale(float _, float t, Transform root, bool isCustomEase, Easing.Type ease, AnimationCurve curve)
        {
            var normalize = _ / maxValue;
            var tween = isCustomEase ? TweenMotion.From(curve, t) : Easing.Interpolate(t, ease);
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (direction)
            {
                case BarDirections.DownToUp:
                case BarDirections.UpToDown:
                    Tweener.Play(root.localScale.y, normalize, tween).SubscribeToLocalScaleY(root).AddTo(root);
                    break;
                default:
                    Tweener.Play(root.localScale.x, normalize, tween).SubscribeToLocalScaleX(root).AddTo(root);
                    break;
            }
        }

        #endregion -- localscale ----------------------------------

        #region -- fillamount ----------------------------------

        /// <summary>
        /// Update fillAmount of image
        /// </summary>
        /// <param name="_">param value</param>
        /// <param name="t">duration update fillAmount</param>
        /// <param name="root">image update fillAmount</param>
        /// <param name="isCustomEase"></param>
        /// <param name="ease"></param>
        /// <param name="curve"></param>
        private void UpdateFillAmount(float _, float t, UnityEngine.UI.Image root, bool isCustomEase, Easing.Type ease, AnimationCurve curve)
        {
            var normalize = _ / maxValue;
            var tween = isCustomEase ? TweenMotion.From(curve, t) : Easing.Interpolate(t, ease);
            Tweener.Play(root.fillAmount, normalize, tween).SubscribeToFillAmount(root).AddTo(root);
        }

        #endregion -- fillamount ----------------------------------

        #region -- position ----------------------------------

        /// <summary>
        /// Update position of transform
        /// </summary>
        /// <param name="_">param value</param>
        /// <param name="t">duration update position</param>
        /// <param name="value">rect of RectTransform contains root</param>
        /// <param name="root">transform bar update position</param>
        /// <param name="isCustomEase"></param>
        /// <param name="ease"></param>
        /// <param name="curve"></param>
        private void UpdatePosition(float _, float t, Rect value, Transform root, bool isCustomEase, Easing.Type ease, AnimationCurve curve)
        {
            var normalize = _ / maxValue;
            var tween = isCustomEase ? TweenMotion.From(curve, t) : Easing.Interpolate(t, ease);
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (direction)
            {
                case BarDirections.DownToUp:
                case BarDirections.UpToDown:
                    Tweener.Play(root.localPosition.y, -value.height + normalize * value.height, tween).SubscribeToLocalPositionY(root).AddTo(root);
                    break;
                default:
                    Tweener.Play(root.localPosition.x, -value.width + normalize * value.width, tween).SubscribeToLocalPositionX(root).AddTo(root);
                    break;
            }
        }

        #endregion -- position ----------------------------------

        #region -- helper ----------------------------------

        // ReSharper disable once MemberCanBeMadeStatic.Local
        /// <summary>
        /// Caculate duration update
        /// </summary>
        /// <param name="_"></param>
        /// <param name="oneUnitValue">value of one unit</param>
        /// <param name="percentIncreate">percent increate of each time</param>
        /// <param name="time">base time</param>
        /// <returns></returns>
        private float CaculateDuration(float _, float oneUnitValue, float percentIncreate, float time)
        {
            var count = Math.Abs(_) / oneUnitValue;
            if (count > 1f)
            {
                return (1 + count * percentIncreate / 100) * time;
            }

            return time * count;
        }

        #endregion -- helper ----------------------------------

        #endregion
    }
}